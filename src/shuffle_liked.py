import datetime
import random
import utilities as ut
from progress_bar import printProgressBar


def get_all_liked_tracks(sp):
    response = sp.current_user_saved_tracks()
    tracks = response["items"]
    print("Fetching songs...")
    while len(tracks) < response["total"]:
        response = sp.current_user_saved_tracks(offset=len(tracks), limit=50)
        tracks.extend(response["items"])
        printProgressBar(len(tracks), response["total"], autosize=True)
    return [t["track"] for t in tracks]


def shuffle_liked_songs(sp):
    tracks = get_all_liked_tracks(sp)
    playlist = ut.make_new_playlist(
        sp, playlist_name="[auto] SHUFFLE",
        description=f"Real shuffle for the people. Auto generated on: {datetime.date.today()}")
    random.shuffle(tracks)
    ut.add_tracks_to_playlist(sp, tracks, playlist["id"])
    print("Playlist generated!")


if __name__ == "__main__":
    shuffle_liked_songs(ut.initialize())
