
# TODO:
#  - rename this file to keys.py
#  - go to https://developer.spotify.com/dashboard and register your app
#  - add http://localhost:5000 and http://localhost:5000/callback to the redirect uris for the app
#  - copy and paste your client id/secret below
#  - modify the app secret to some complex random string

SPOTIPY_CLIENT_ID = "CLIENT_ID_HERE"
SPOTIPY_CLIENT_SECRET = "CLIENT_SECRET_HERE"
SPOTIPY_REDIRECT_URI = "http://localhost:5000"
SPOTIPY_SCOPE = "user-library-read playlist-modify-public"
APP_SECRET = "some_string"
