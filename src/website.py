import time
from flask import Flask, render_template, request, Response, redirect, session
import spotipy
from shuffle_liked_thread import ShuffleThread
from suggestions_thread import SuggestionThread

# TODO: replace these with environment variables....
from keys import *

app = Flask(__name__, template_folder="../templates",
            static_folder="../static")
app.secret_key = APP_SECRET


def get_oauth():
    return spotipy.oauth2.SpotifyOAuth(
        client_id=SPOTIPY_CLIENT_ID,
        client_secret=SPOTIPY_CLIENT_SECRET,
        redirect_uri="http://localhost:5000/callback",
        scope=SPOTIPY_SCOPE,
        show_dialog=True)


def logged_in(session):
    return bool(session.get('token_info', False))


def refresh_token(session):
    if not logged_in(session):
        return False

    now = int(time.time())
    token_expired = session['token_info']['expires_at'] - now < 60
    if token_expired:
        print("refreshed token!")
        session['token_info'] = get_oauth().refresh_access_token(
            session['token_info']['refresh_token'])
    return True


def get_sp():
    if refresh_token(session):
        return spotipy.Spotify(auth=session['token_info']['access_token'])
    else:
        return None


def get_user_info(session):
    if not logged_in(session):
        return {"logged_in": False}
    else:
        sp = get_sp()
        user = sp.current_user()
        return {"logged_in": True, "profile_url": user['external_urls']['spotify'], "image": user['images'][0]['url']}


@app.route('/')
@app.route('/home')
def home():
    return render_template("home.html", **get_user_info(session))


@app.route('/randomize', methods=['GET', 'POST'])
def randomize():
    if request.method == 'GET':
        return render_template("randomize.html", **get_user_info(session))
    else:
        if not logged_in(session):
            return render_template("error.html", **get_user_info(session))
        else:
            session['randomize_playlist_name'] = request.form['playlist_name']
            return render_template("progress.html", **get_user_info(session))


@app.route('/progress')
def progress():
    t = ShuffleThread(get_sp(), session['randomize_playlist_name'])
    t.start()

    def generate():
        while t.progress <= 100:
            yield "data:" + str(t.progress) + "\n\n"
            time.sleep(0.5)
    return Response(generate(), mimetype='text/event-stream')


@app.route('/login')
def login():
    return redirect(get_oauth().get_authorize_url())


@app.route('/callback')
def callback():
    sp_oauth = get_oauth()
    session.clear()
    session['token_info'] = sp_oauth.get_access_token(request.args.get('code'))
    return redirect('/home')


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/home')


@app.route('/suggestions', methods=['GET', 'POST'])
def suggestions():
    if request.method == 'GET':
        genre_list = get_sp().recommendation_genre_seeds()["genres"]
        return render_template("suggestions.html", genre_list=genre_list)
    else:
        if not logged_in(session):
            return render_template("error.html", **get_user_info(session))
        else:
            args = request.form.to_dict(flat=False)
            args = {k: list(filter(None, v)) for k, v in args.items()}
            SuggestionThread(get_sp(), args).start()
            return render_template("home.html", **get_user_info(session))


if __name__ == "__main__":
    app.run(debug=True)
