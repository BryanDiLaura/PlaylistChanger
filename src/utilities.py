import spotipy
from spotipy.oauth2 import SpotifyOAuth

import keys


def initialize():
    scope = "user-library-read playlist-modify-public"

    return spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope, client_id=keys.SPOTIPY_CLIENT_ID,
                                                     client_secret=keys.SPOTIPY_CLIENT_SECRET, redirect_uri=keys.SPOTIPY_REDIRECT_URI))


def make_new_playlist(sp, *, playlist_name, description):
    user = sp.current_user()

    # unfollow if we already have an instance of the playlist
    for playlist in sp.current_user_playlists()["items"]:
        if playlist_name in playlist["name"]:
            sp.user_playlist_unfollow(
                user=user["id"], playlist_id=playlist["id"])
            break

    return sp.user_playlist_create(user=user["id"], name=playlist_name,
                                   description=description)


def add_tracks_to_playlist(sp, track_list, playlist_id):
    # can only add 100 tracks at a time...
    def chunks(lst, n):
        for i in range(0, len(lst), n):
            yield lst[i:i+n]

    user = sp.current_user()
    for tracks in chunks(track_list, 100):
        sp.user_playlist_add_tracks(
            user=user["id"], playlist_id=playlist_id, tracks=[t["uri"] for t in tracks])
