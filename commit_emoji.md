I am using a simplified version of (gitmoji)[https://gitmoji.dev/] for this project. 
The following is the key:

| emoji |    code    | meaning              |
| :---: | :--------: | :------------------- |
|   ✨   | :sparkles: | New code or features |
|   🎨   |   :art:    | Refactors            |
|   🐛   |   :bug:    | Fix a bug            |
|   📚   |  :books:   | Documentation        |
|   🔧   |  :wrench:  | Tooling              |
|   💄   | :lipstick: | UI                   |