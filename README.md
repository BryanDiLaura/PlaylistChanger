# Spotify Helper

## Synopsis
Spotify Helper is a python-based tool for filling in functionality that I felt was missing in Spotify. 
This is mostly an excuse to play around with the Spotify API and web development, but with the goal of 
making something that is actually useful too! 

This project comes in two parts: a web interface that uses a Bootstrap frontend 
with a Flask backend, and a few command-line tools. Both leverage the 
[Spotipy](https://github.com/plamere/spotipy) project for easier communication
from Python and Spotify.

## Getting Started

### Running
Assuming you have everything correctly set up (see [setup](#setup)):
- Run `pipenv run -- python3 src/website.py`
  - Note: On first run this will install all dependecies
- Open your web browser and navigate to [http://localhost:5000](http://localhost:5000)


## Setup
### Dependencies
To use Spotify Helper, you must have a machine with the following installed:
- [Python3 with pip](https://www.python.org/downloads/) (Download and install)
- [pipenv](https://pipenv.pypa.io/en/latest/) (Run `pip install pipenv`)

### Spotify Prerequisites
This tool talks to the Spotify API, which means you need to get a developer API key. This can be 
done via the following:

- Go to the [Spotify developer portal](https://developer.spotify.com/dashboard) and log in
- Accept the Developer Terms of Service
- Click "Create An App", and give it some name and description
- Click "Users And Access" and add any users by their spotify email
- Click "Edit Settings" and add a Redirect URI, for `http://localhost:5000` and `http://localhost:5000/callback`
- Click "Show Client Secret"
- In the repository: 
  - Rename `src/keys_template.py` to `src/keys.py`
  - Replace the client ID and secret with yours

## Contributors
- [Bryan DiLaura](bryan.dilaura@gmail.com)

## License
Licensed under a MIT license. For more details, please see LICENSE.txt.
